# コンピュータビジョン研究室のWebページ

## 公開方法

- 普通のコンテンツは　CI/CD で勝手に公開される．
- 学内限定で公開するコンテンツだけは大学 Web サーバーに別途 rsync する．
- 学内限定コンテンツは Google Drive の Kindai_CVlab -> webpage -> labweb にある



