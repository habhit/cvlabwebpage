document.write('\
<div class="footer">\
<div class="container-fluid">\
  <div class="row" id="infooter">\
    <div class="col-sm-1"></div>\
    <div class="col-sm-3">\
      <p>リンク</p>\
      <ul style="list-style:none">\
       <li><a href="https://github.com/KindaiCVLAB">GitHub</a></li>\
      </ul>\
    </div>\
    <div class="col-sm-3">\
      <p>学内関連リンク</p>\
      <ul style="list-style:none">\
        <li><a href="http://www.kindai.ac.jp/">近畿大学</a></li>\
        <li><a href="http://www.kindai.ac.jp/sci/">理工学部</a></li>\
        <li><a href="http://www.info.kindai.ac.jp/informatics/">情報学科</a></li>\
      </ul>\
    </div>\
    <div class="col-sm-3">\
      <p>学外関連リンク</p>\
      <ul style="list-style:none">\
        <li><a href="http://www.am.sanken.osaka-u.ac.jp/index-jp.html">大阪大学・八木研究室</a></li>\
        <li><a href="http://isw3.naist.jp/home-ja.html">奈良先端科学技術大学院大学 情報科学研究科</a></li>\
        <li><a href="http://cvim.ipsj.or.jp/">情報処理学会CVIM研究会</a></li>\
        <li><a href="http://www.ieice.org/~prmu/jpn/">電子情報通信学会PRMU研究会</a></li>\
      </ul>\
    </div>\
  </div>\
</div>\
<div class="copyright"> Copyright (c) 2020 Computer Vision Lab., Department of Informatics, Faculty of Science and Technology, Kindai University </div>\
</div>\
');
