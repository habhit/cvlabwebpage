document.write('\
		<div id="menuR">\
			<!-- #menuR 右サイドメニュー -->\
			<div class="subinfo">\
				<div class="label">リンク</div>\
				<ul>\
					<li><a href="http://www.habe-lab.org/public_wiki/doku.php/start">CVLAB公開ノウハウ</a></li>\
				</ul>\
				<div class="label">学内関連リンク</div>\
				<ul>\
					<li><a href="http://www.kindai.ac.jp/">近畿大学</a></li>\
					<li><a href="http://www.kindai.ac.jp/sci/">理工学部</a></li>\
					<li><a href="http://www.info.kindai.ac.jp/infomatics/">情報学科</a></li>\
				</ul>\
			</div>\
			<div class="subinfo">\
				<div class="label">学外関連リンク</div>\
				<ul>\
					<li><a href="http://vision.kuee.kyoto-u.ac.jp/japanese/index.html">京都大学・松山研究室</a></li>\
					<li><a href="http://www.am.sanken.osaka-u.ac.jp/index-jp.html">大阪大学・八木研究室</a></li>\
					<li><a href="http://isw3.naist.jp/home-ja.html">奈良先端科学技術大学院大学 情報科学研究科</a></li>\
					<li><a href="http://cvim.ipsj.or.jp/">情報処理学会CVIM研究会</a></li>\
					<li><a href="http://www.ieice.org/~prmu/jpn/">電子情報通信学会PRMU研究会</a></li>\
				</ul>\
			</div>\
			<div class="subinfo">\
			<div class="label">CVLABの日常</div>\
				<p>\
');
